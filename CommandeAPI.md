## Développement d'une ressource Pizza

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource pizzas. Celle-ci devrait répondre aux URI suivantes :

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                          |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------------------|
| /pizzas                  | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (I2)                                            |
| /pizzas/{id}             | GET         | <-application/json<br><-application/xml                      |                 | une pizza (I2) ou 404                                            |
| /pizzas/{id}/name        | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizzas/{id}/ingredients | GET         | <-application/json<br><-application/xml                      |                 | une list d'ingrédients de la pizza (I2) ou 404                                            | /pizzas                  | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (I1)      | Nouvelle pizza (I2)<br>409 si la pizza existe déjà (même nom)    |
| /pizzas/{id}             | DELETE      |