## Développement d'une ressource Commande

### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource commandes. Celle-ci devrait répondre aux URI suivantes :

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                             |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :-------------------------------------------------------------------|
| /commandes               | GET         | <-application/json<br><-application/xml                      |                 | liste des commandes (I2)                                            |
| /commandes/{id}          | GET         | <-application/json<br><-application/xml                      |                 | une commande (I2) ou 404                                            |
| /commandes/{id}/name     | GET         | <-text/plain                                                 |                 | le nom de la commande ou 404                                        |
| /commandes/{id}/pizzas   | GET         | <-application/json<br><-application/xml                      |                 | une list des pizzas de la commande(I2) ou 404                       |
| /commandes               | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Commande (I1)   | Nouvelle commande (I2)<br>409 si la commande existe déjà (même nom) |
| /commandes/{id}          | DELETE      |