package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzasCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzasDto;

public class Pizzas {
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizzas() {this.ingredients = new ArrayList<Ingredient>();}
	public Pizzas(String name) {this.name = name; this.ingredients = new ArrayList<Ingredient>();}
	public Pizzas(String name, UUID id) {this.name = name; this.id = id; this.ingredients = new ArrayList<Ingredient>();}
	public Pizzas(String name, UUID id, List<Ingredient> oui) {this.name = name; this.id = id; this.ingredients = oui;}
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void addOneIngredient(Ingredient I) {
		this.ingredients.add(I);
	}
	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	@Override
	public String toString() {
		return "Pizzas [id=" + id + ", name=" + name + ", ingredients=" + ingredients + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ingredients == null) ? 0 : ingredients.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizzas other = (Pizzas) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	public static PizzasDto toDto(Pizzas p) {
		PizzasDto dto = new PizzasDto();
		dto.setId(p.getId());
		dto.setName(p.getName());

		return dto;
	}

	public static Pizzas fromDto(PizzasDto dto) {
		Pizzas pizza = new Pizzas();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());

		return pizza; 
	}
	public static Pizzas fromPizzasCreateDto(PizzasCreateDto pizzasCreateDto) {
	    Pizzas pizza = new Pizzas();
	    pizza.setName(pizzasCreateDto.getName());
	    return pizza;
	  }
	public static PizzasCreateDto toCreateDto(Pizzas pizza) {
        PizzasCreateDto dto = new PizzasCreateDto();
        dto.setName(pizza.getName());

        return dto;
    }
	
}
