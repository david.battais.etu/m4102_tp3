package fr.ulille.iut.pizzaland.dao;


import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizzas;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idP VARCHAR(128), idI VARCHAR(128), constraint fk_idP foreign key(idP) references Pizzas(id), constraint fk_idI foreign key(idI) references Ingredients(id))")
	void createAssociationTable();

	@SqlUpdate("DROP TABLE IF EXISTS Pizzas")
	void dropTablePizzas();

	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
	void dropTablePIA();
	@Transaction
	default void createTablePizzaAndIngredientAssociation() {
		createPizzaTable();
		createAssociationTable();
	}
	@Transaction
	default void dropTableAndIngredientAssociation() {
		dropTablePIA();
		dropTablePizzas();
	}

	@Transaction
	default void deletePizzasByID(UUID id) {
		removeAssociation(id);
		removePizzas(id);
	}

	@Transaction
	default void insertIntoPizzas(Pizzas pizza) {
		insertPizzas(pizza);
		for (Ingredient ingredient : pizza.getIngredients()) {
			insertAssociation(pizza.getId(),ingredient.getId());
		}    	
	}
	
	@Transaction
	default Pizzas getFromId(UUID id) {
		IngredientDao ingredients = BDDFactory.buildDao(IngredientDao.class);
		Pizzas p = findById(id);
		List<UUID> idList= findByIdIngredient(id);
		for(UUID idI : idList) {
			p.addOneIngredient(ingredients.findById(idI));
		}
		return p;
	}

	@SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE idP = :id")
	void removeAssociation(@Bind("id") UUID id);

	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
	void removePizzas(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizzas.class)
	List<Pizzas> getAll();

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizzas.class)
	Pizzas findById(@Bind("id") UUID id);
	
	@SqlQuery("SELECT idI FROM PizzaIngredientsAssociation WHERE idP = :id")
	List<UUID> findByIdIngredient(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizzas.class)
	Pizzas findByName(@Bind("name") String name);

	@SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
	void insertPizzas(@BindBean Pizzas pizzas);

	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idP, idI) VALUES (:idP, :idI)")
	void insertAssociation(@Bind("idP") UUID idP, @Bind("idI") UUID idI);
}
