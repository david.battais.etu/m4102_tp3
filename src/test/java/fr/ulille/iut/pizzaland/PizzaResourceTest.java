package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizzas;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzasCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzasDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest{
	private PizzaDao dao;
	private IngredientDao dao2;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao2 = BDDFactory.buildDao(IngredientDao.class);
		dao2.createTable();
		dao.createTablePizzaAndIngredientAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAndIngredientAssociation();
		dao2.dropTable();
	}
	@Test
    public void testGetEmptyList() {
        // La méthode target() permet de préparer une requête sur une URI.
        // La classe Response permet de traiter la réponse HTTP reçue.
        Response response = target("/pizzas").request().get();

        // On vérifie le code de la réponse (200 = OK)
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        // On vérifie la valeur retournée (liste vide)
        // L'entité (readEntity() correspond au corps de la réponse HTTP.
        // La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
        // de la réponse lue quand on a un type paramétré (typiquement une liste).
        List<PizzasDto> pizzas;
        pizzas = response.readEntity(new GenericType<List<PizzasDto>>() {
        });

        assertEquals(0, pizzas.size());

    }
	@Test
	public void testGetExistingPizzas() {
		Pizzas pizza = new Pizzas();
		pizza.setName("Chorizo");
		dao.insertPizzas(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		//Pizzas result = Pizzas.fromDto(response.readEntity(PizzasDto.class));
		Pizzas result = dao.findById(pizza.getId());
		assertEquals(pizza, result);
	}
	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	@Test
	public void testCreatePizzas() {
		Pizzas pizza = new Pizzas();
		pizza.setName("popail");
		Ingredient i = new Ingredient("epinard");
		dao2.insert(i);
		dao2.insert(new Ingredient("tomate"));
		List<Ingredient> ing = new ArrayList<>();
		ing = dao2.getAll();
		pizza.setIngredients(ing);
		dao.insertIntoPizzas(pizza);

		assertEquals(pizza, dao.getFromId(pizza.getId()));
	}

	@Test
	public void testCreateSamePizzas() {
		Pizzas pizza = new Pizzas();
		pizza.setName("popeye");
		dao2.insert(new Ingredient("petitpois"));
		List<Ingredient> ing = new ArrayList<>();
		ing = dao2.getAll();
		pizza.setIngredients(ing);
		dao.insertPizzas(pizza);

		PizzasCreateDto pizzasCreateDto = Pizzas.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzasCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzasCreateDto pizzaCreateDto = new PizzasCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	@Test
	public void testDeleteExistingPizzas() {
		Pizzas pizza = new Pizzas();
		pizza.setName("popeye");
		dao2.insert(new Ingredient("tomate"));
		dao2.insert(new Ingredient("fromage"));
		List<Ingredient> ing = new ArrayList<>();
		ing = dao2.getAll();
		pizza.setIngredients(ing);
		dao.insertIntoPizzas(pizza);
		System.out.println("\n\n\n"+pizza.getIngredients()+"\n\n\n");

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizzas result = dao.findById(pizza.getId());
		assertEquals(result, null);
	}
	
	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	@Test
	public void testGetPizzaName() {
		Pizzas pizza = new Pizzas();
		pizza.setName("popeye");
		dao2.insert(new Ingredient("tomate"));
		dao2.insert(new Ingredient("fromage"));
		List<Ingredient> ing = new ArrayList<>();
		ing = dao2.getAll();
		pizza.setIngredients(ing);
		dao.insertIntoPizzas(pizza);

		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("popeye", response.readEntity(String.class));
	}
	@Test
	public void testGetNotExistingPizzasName() {
		Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	@Test
	public void testGetIngredientNameFromPizzas() {
		Pizzas pizza = new Pizzas();
		pizza.setName("popail");
		Ingredient i = new Ingredient("epinard");
		dao2.insert(i);
		dao2.insert(new Ingredient("tomate"));
		List<Ingredient> ing = new ArrayList<>();
		ing = dao2.getAll();
		pizza.setIngredients(ing);
		dao.insertIntoPizzas(pizza);
		System.out.println("\n\n\n"+dao.findById(pizza.getId())+"\n\n\n");
		
		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		String name = "epinard";
		assertEquals(name, dao.getFromId(pizza.getId()).getIngredients().get(0).getName());
	} 
	/*
	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "chorizo");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("ingredients").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Ingredient result = dao.findById(UUID.fromString(id));
		assertNotNull(result);
	}*/
}
